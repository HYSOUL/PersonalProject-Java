package junitTest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import wCount.FileDeal;
import wCount.WordDeal;

public class WordDealTest {

	@Before
	public void setUp() throws Exception {
		
	}

	@Test
	public void testGetCharCount() throws IOException {//统计字符数量测试
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");	
	
		WordDeal wd1 = new WordDeal(text1);
	
		int cn1 = wd1.getCharCount();
	
	}
	
	@Test
	public void testGetWordCount() throws IOException {//统计单词数量测试
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		WordDeal wd1 = new WordDeal(text1);
		int wn1 = wd1.getWordCount();
		
	}

	@Test
	public void testGetWordFreq() throws IOException {//统计词频测试
		
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		WordDeal wd1 = new WordDeal(text1);
		List wf1 = wd1.getWordFreq();
		
	}


	@Test
	public void testGetLineCount() throws IOException {//统计有效行数测试
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		WordDeal wd1 = new WordDeal(text1);
		int wn1 = wd1.getLineCount();
	
	}

	@Test
	public void testListToArray() throws IOException {
		
		
		FileDeal fd = new FileDeal();
		String text1 = fd.FileToString("text/text1.txt");
		WordDeal wd1 = new WordDeal(text1);
		List wf1 = wd1.getWordFreq();
		String[] s1 = wd1.ListToArray(wf1);
	}

}
