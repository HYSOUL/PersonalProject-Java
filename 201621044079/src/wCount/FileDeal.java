package wCount;

import java.io.*;

public class FileDeal { //文件处理类

	public String FileToString(String path) throws IOException { //文件读取
		File file = new File(path);
		if (!file.exists() || file.isDirectory()) {
			System.out.println("请输入正确文件名！");
			throw new FileNotFoundException();
		}
		FileInputStream fis = new FileInputStream(file);
		byte[] buf = new byte[1024];
		StringBuffer sb = new StringBuffer();
		while ((fis.read(buf)) != -1) {
			sb.append(new String(buf));
			buf = new byte[1024];
		}
		return sb.toString();
	}

	public void WriteToFile(String str) throws IOException { // 文件写入
		File writename = new File("result.txt"); 
		writename.createNewFile(); // 创建新文件
		BufferedWriter out = new BufferedWriter(new FileWriter(writename));
		out.write(str);
		out.flush(); 
		out.close(); 

	}

}
